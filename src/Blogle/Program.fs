﻿// Learn more about F# at http://fsharp.org

open System
open Suave
open Suave.Http
open Suave.Web

[<EntryPoint>]
let main argv =
    printfn "Hello World from F#!"
    let ip = System.Net.IPAddress.Parse "0.0.0.0"
    let conf = {defaultConfig with bindings=[HttpBinding.create HTTP ip 8080us]}
    startWebServer conf (Successful.OK "Hello World!")    
    0 // return an integer exit code
