FROM fsharp:netcore AS build
WORKDIR /app

COPY . /app/

RUN dotnet restore

RUN dotnet build -c Release -o out

EXPOSE 8080

ENTRYPOINT ["dotnet", "src/Blogle/out/Blogle.dll"]